﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventuresOfDad
{
    internal class Input
    {
        private readonly Movement _movement;
        private readonly Player _player;
        private readonly Draw _draw;
        public Input(Movement movement, Player player, Draw draw)
        {
            _movement = movement;
            _player = player;
            _draw = draw;
            GetInput();
        }

        public Input()
        {
            
        }
        internal void GetInput()
        {
            while (true)
            {
                switch (Console.ReadKey().Key)
                {
                    
                    case ConsoleKey.UpArrow:
                        _movement.MoveUp();
                        _draw.Update();
                        break;
                    case ConsoleKey.DownArrow:
                        _movement.MoveDown();
                        _draw.Update();
                        break;
                    case ConsoleKey.LeftArrow:
                        _movement.MoveLeft();
                        _draw.Update();
                        break;
                    case ConsoleKey.RightArrow:
                        _movement.MoveRight();
                        _draw.Update();
                        break;
                    case ConsoleKey.Spacebar:
                        _player.Attack();
                        _draw.Update();
                        break;
                    
                }
            }
        }
    }
}
